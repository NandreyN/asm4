/*1. ��� ���������� �������:

- ����� ��� ��������� ���������, (��������� �������� - ������� �������, ��� �������� �������� �������� �� �������� �� ����������� ������ �������);

- ������������� ������ �� ����������� (������������ ������-��������� ���������);

- ����������� ������ ���, ����� ��� ���� ������������� �� ����������� ������������� ��������.
*/

#include <iostream>
#include <iomanip>

using namespace std;

template<class T>
void print(T ptr , int dim)
{
	for (int i = 0; i < dim; i++)
	{
		for (int j = 0; j < dim; j++)
			cout << setw(3) << ptr[i][j] << " ";
		cout << endl;
	}
}

extern "C" int getMax(int* p, int dim)
{
	int max = p[0];
	_asm
	{
		pusha

		mov ecx, dim
		dec ecx // to repeat until n-1 element 

		mov esi , 1 // index
		mov ebx, p // mov base to ebx register

		find_maximum_label:
			mov eax, [ebx + esi * 4] // get elemnt from the array
			cmp eax , max // compare with greatest one so far
			jle find_maximum_continueIterationue
			mov max, eax // save maximum

			find_maximum_continueIterationue : 
			inc esi // next element
		loop find_maximum_label
		
		popa
	}
	return max;
}

extern "C" void swapRows(int* a, int* b, int dim)
{
	int i = 0;
	_asm
	{
		pusha

		mov eax, [a]
		mov ebx , [b] // move bases
		mov ecx, dim
		mov esi , 0
		swapCells:
			push ecx
			mov ecx , int ptr [eax  + esi * 4]
			mov edx , int ptr [ebx + esi * 4]
			mov int ptr [eax + esi * 4], edx
			mov int ptr [ebx + esi * 4], ecx
			pop ecx
			inc esi
		loop swapCells

		popa
	}
}

int main()
{
	int dim = 6;
	int a[6][6] = { {1,2,5,7,2,33},
					{4,-5,8,1,-2,0},
					{7,30,4,2,1,2},
					{9,2,3,1,11,2},
					{2,7,8,9,0,0},
					{20,47,11,1,0,1}};
	int binary[6][6] = { 0 };

	cout << "Initial array : " << endl;
	print<int[6][6]>(a, dim);

	int i = 0,j = 0, bytes = 0;
	int directionsBuffer = 0;
	// [BOTTOM|TOP|RIGHT|LEFT]
	_asm
	{
		 mov eax, dim
		 imul eax
		 mov ecx , eax 
		 // above : calculating number of repeatings
		 mov eax, 4
		 imul dim
		 mov bytes, eax
		 // how many bytes to skip to get to the next line with the same column number
		 cycle:
		 // calculating current position in array
		 mov eax,i
		 imul dim
		 add eax, j
		 mov ebx, 4
		 imul ebx
		 mov esi, eax
		 // get value of current cell
		 mov ebx, a[esi]
		 // set directionsBuff to 0
		 mov directionsBuffer , 0
		 // can move left check
		 cmp j, 0
	     je	moveRigthCheck	 
		 // get left one
		 mov BYTE ptr [directionsBuffer], 1
		 mov edx, a[esi - 4]
		 cmp edx, ebx
		 jg continueIteration

		 // move rigth check
		moveRigthCheck:
			 mov edx,j
			 inc edx
			 cmp edx, dim
			 je moveUpCheck
			 // get right
			 mov BYTE ptr[directionsBuffer + 1], 1
			 mov edx, a[esi + 4]
		cmp edx, ebx
		jg continueIteration
			
		// get upper check
		moveUpCheck:
			 cmp i, 0
			 je moveDownCheck
		// get upper
		mov BYTE ptr[directionsBuffer + 2], 1
		sub esi, bytes
		mov edx, a[esi]
		add esi , bytes
		cmp edx, ebx
		jg continueIteration

		// get down check
		moveDownCheck:
			mov edx,i
			inc edx
			cmp edx, dim
			je recalculateIndex // !!!
			 // get down
			mov BYTE ptr[directionsBuffer + 3], 1
			add esi , bytes 
			mov edx, a[esi]
			sub esi, bytes
			cmp edx, ebx
			jg continueIteration
		// now coner checks
		// calc index first , and then we will add and sub dimbytes 
		// ranther than recalcualting in each corner 
		recalculateIndex:		mov eax, i
				dec eax
				imul dim
				mov edx, j
				dec edx
				add eax, edx
				mov edx, 4
				imul edx
				mov esi , eax
		// left-upper 0 2
			//push esi

				leftTopCheck: 
			mov al, BYTE ptr [directionsBuffer]
			mov ah, BYTE ptr [directionsBuffer + 2]
			and al, ah
			je rightTopCheck 
			// else get left-top value
			mov edx, a[esi]
			cmp edx, ebx
			jg continueIteration
	

			rightTopCheck :
			// move esi
			add esi , 8
			xor eax, eax
			xor edx, edx
			mov al, BYTE ptr[directionsBuffer + 1]
			mov ah, BYTE ptr[directionsBuffer + 2]
			and al, ah
			je rightBottomCheck

			mov edx, a[esi]
			cmp edx, ebx
			jg continueIteration

			rightBottomCheck:
			add esi , bytes
			add esi, bytes
				xor eax, eax
				xor edx, edx
				mov al, BYTE ptr[directionsBuffer + 1]
				mov ah, BYTE ptr[directionsBuffer + 3]
				and al, ah
				je leftBottomCheck

				mov edx, a[esi]
				cmp edx, ebx
				jg continueIteration
			
			leftBottomCheck:
			sub esi , 8 
				xor eax, eax
				xor edx, edx
				mov al, BYTE ptr[directionsBuffer]
				mov ah, BYTE ptr[directionsBuffer + 3]
				and al, ah
				je set_mark
				
				mov edx, a[esi] // here
				cmp edx, ebx
				jg continueIteration
				
			set_mark : 
			mov eax, i
			imul dim
			add eax , j
			mov ebx, 4
			imul ebx
			mov binary[eax], 1
		 continueIteration:
		 // down: chnging indexes
		 inc j
		 mov eax, j
		 cmp eax, dim
		 jne newcycle
		 inc i
		 mov j , 0
			 newcycle:
		 dec ecx
		 // above: changing indexes
			cmp ecx, 0
		 jg cycle
	}
	cout << endl << "Maximums : " << endl;
	print<int[6][6]>(binary, dim);

	//- ������������� ������ �� �����������(������������ ������ - ��������� ���������);
	_asm
	{
		// ebx - first index
		mov j, 0
		mov i, 0
		mov ecx, dim
		
		i_c:
			push ecx

			mov ecx, dim
			k_c :
				push ecx

				mov ecx, dim
				dec ecx
				mov esi , 0

				mov j , 0
				j_cycle:
					mov eax, dim
					imul i
					add eax , j
					mov ebx , 4
					imul ebx
					mov esi ,eax

					lea edx, a
					mov eax , int ptr [edx][esi]
					add esi ,4
					mov ebx , int ptr [edx][esi]
					cmp eax , ebx
					jle c
	
					mov	int ptr[edx][esi], eax
					sub esi ,4
					mov int ptr [edx][esi], ebx

					c:inc j
				loop j_cycle

				pop ecx
			loop k_c

			pop ecx
			inc i
		loop i_c
	}
	cout << "Eaxh line was sorted: " << endl;
	print<int[6][6]>(a, dim);

	int max1 = 0;
	_asm
	{ 
		// Sorting cycles:
		mov ecx, dim
		kk_c:
			mov i , 0
			push ecx

			mov ecx ,dim
			dec ecx
			mov esi , 0 // Index without scaling
			ii_c:
				push ecx
				// Get current index of the first element of the string
				mov eax, 4
				imul i
				imul dim
				mov esi ,eax
				// Perform getMax function call for (i) row 
				// Not needed here, array rows were sorted sorted
				// For random matrix - ok
				push dim
				lea eax, [a + esi]
				push eax
				call getMax
				pop ebx // junk
		
				// result is in eax
				mov max1, eax
				// Perform fucntion call for (i + 1) row
				lea eax , [a + esi]
				add eax , bytes
				push eax
				call getMax
				pop ebx // junk
				pop ebx // junk
				// res in eax
				cmp max1, eax
				jle cn
				// Else swap
				push ebx // dim is here after last pop above 
				lea eax, [a + esi]
				push eax
				add eax , bytes
				push eax
				call swapRows
				pop ebx
				pop ebx
				pop ebx

				cn: 
					inc i
					pop ecx
			loop ii_c

			pop ecx
		loop kk_c
	}
	cout << "Sorted by rows :(array above) " << endl;
	print<int[6][6]>(a, dim);
	return 0;
}