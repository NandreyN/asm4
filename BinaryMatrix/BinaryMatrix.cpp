//2. ��� ������� �� 0 � 1
// ����� ������ � ������� � ������������ ����������� ������ ������ ������

#include <iostream>
#include <iomanip>
#include <bitset>
#include <windows.h>
using namespace std;

template<class T>
void print(T ptr, int dim)
{
	for (int i = 0; i < dim; i++)
	{
		for (int j = 0; j < dim; j++)
			cout << setw(3) << ptr[i][j] << " ";
		cout << endl;
	}
}

void fillRange(int* range , BYTE fillWith)
{
	int dim = 8;
	_asm
	{
		mov ecx , dim
		
		put_range:
			mov esi , ecx
			dec esi
			mov bl, fillWith
			and bl , 1
			cmp bl , 0 // remove it!!
			je co
			mov eax , [range]
			mov int ptr [eax + esi * 4], 1

		co:
			shr fillWith , 1
		loop put_range
	}
}

int getOneCount(int* p , int dim, int step)
{
	// just moving across the line and incrementing counter ,if current digit is 1
	// and providing comparasion with previous result in case of 0
	int count = 0;
	_asm
	{

		mov ecx, dim
		mov esi , 0
		mov eax, [p]
		mov edx , 0

		countOne:
		mov ebx , [eax + esi * 4]
		cmp ebx , 0
		jne one

		zero:
		cmp edx , count
		jle j
		mov count , edx
		j:mov edx , 0
		jmp con

		one:
		inc edx
		con : add esi , step
		loop countOne

		cmp edx, count
		jle  exit_asm1
		mov count, edx
			exit_asm1:
	}
	return count;
}


int main()
{
	int dimNum = 2 ;
	BYTE numeric[16][2] {{21, 100},
					   {112, 123},
					   {7,33},
					   {-65,-2},
					   {14,31},
					   {10,1},
					   {0,1},
					   {74,31},
					   { 9, 100 },
					   { 112, 123 },
					   { 7,33 },
					   { -65,31 },
					   { 23,31 },
					   { 10,1 },
					   { 0,1 },
					   { 98,31 }
	};
	int a[16][16] = {0};

	for (int i = 0;i< dimNum * 8;i++)
	{
		for (int j = 0; j < dimNum;j++)
		{
			std::bitset<8> x(numeric[i][j]);
			cout << x;
		}
		cout << endl;
	}

	int dim = dimNum * 8;
	int i = 0, j = 0;
	_asm
	{
		mov ecx, dim
		row:
		push ecx

		mov ecx, dimNum 
			columns:
		push ecx
		// iterate through elements of decimal array 
		// calculate index
		mov eax , i
		imul dimNum
		add eax, j
		mov esi , eax 
		// index got, get element
		mov bl , BYTE ptr [numeric + esi * 1]
		// push binary representation to the corresponding range 
		// get start point of the range
		// i*dim*4 + (j * 8)*4
		mov eax , 4
		imul dim
		imul i
		push eax // to store somewhere

		mov eax , 32
		imul j
		pop edx
		add eax , edx
		// index is in eax
		// organize function call
		// get adress
		lea edx , a
		add edx , eax
		push bl
		push edx
		call fillRange // put byte to it`s position in binary matrix
		pop edx // junk
		pop edx // junk

		
		inc j
		pop ecx
		loop columns

		pop ecx
		mov j , 0
			inc i
		loop row
	}
	//print<int[16][16]>(a, dim);

	/*int a[16][16] = { 0 };
	/*int a[5][5] = { {1,0,0,1,1},
					{1,1,1,0,0},
					{0,0,0,0,0},
					{1,1,1,1,0},
					{1,0,0,0,1}};*/
	/*int a[2][2] = {{1,1},
					{0,1}};*/
	/*int a[3][3] = { {1,1,1},
					{0,1,0},
					{0,0,1}};*/
	pair<int, int> row, column; // first  - index, second - count. In asm work with second
	int dimBytes;
	_asm
	{
		mov eax , 4
		imul dim 
		mov dimBytes ,eax

		mov ecx , dim
		mov esi , 0

		calc:
		// columns first
			push ecx
			push dim
			push dim
			lea eax, a[esi*4] // get address of raw first element
			push eax

			call getOneCount
			pop ebx 
			pop ebx
			pop ebx

			cmp eax , column.second
			jle rows
			mov column.first , esi
			mov column.second , eax

			rows:
			// then rows
			push 1 // step 
			push dim
			mov eax ,4 
			imul esi
			imul dim // get index of row begining
			lea eax , a[eax]
			push eax
			call getOneCount

			pop ebx
			pop ebx
			pop ebx

			cmp eax, row.second
			jle c_cont
			mov row.first, esi
			mov row.second, eax

			c_cont: inc esi
			pop ecx	
		loop calc 
	}
	print<int[16][16]>(a,dim);
	cout << "Row #" << row.first << ", count = " << row.second << endl;
	cout << "Column #" << column.first << ", count = " << column.second << endl;
	return 0;
}